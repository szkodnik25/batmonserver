from pymongo import MongoClient
import cherrypy
import json

class AppsDataBase():
    def __init__(self):
        client = MongoClient()
        db = client.batmon_database

        self.collection = db.batmon_collection
        self.applications = db.applications

    def add_new_one(self, package, name, permisisons, mobileRxFg, mobileRxBg, wlanRxFg, wlanRxBg, num):
	if self.applications.find_one({"_id": package}):
		return "exist"
        json = {"_id": package,
                "name": name,
                "permissions": permisisons.split(),
		"uid": '0',
                "mobileRxFg": mobileRxFg,
                "mobileRxBg": mobileRxBg,
		"wlanRxFg": wlanRxFg,
		"wlanRxBg": wlanRxBg,
                "num": num}
        return self.applications.insert_one(json).inserted_id

    def add_many(self, json_array):
        return self.applications.insert_many(json_array).inserted_ids

    def get_by_package(self, package):
        return self.applications.find_one({"_id": package})

    def get_all(self):
        return list(self.applications.find())

    def remove_by_package(self, package):
        self.applications.remove({"_id": package})

class BatMonWebService(object):
    exposed = True
    cherrypy.config.update({'server.socket_host' : '192.168.1.16',
				            'server.socket_port' : 8080})
    def __init__(self):
        self.apps = AppsDataBase()

    @cherrypy.tools.accept(media='text/plain')
    def GET(self, package):
        if package == 'ANY':
            return json.dumps(self.apps.get_all())
        return str(self.apps.get_by_package(package))

    def POST(self, package, name, permissions, mobileRxFg, mobileRxBg, wlanRxFg, wlanRxBg, num):
        return self.apps.add_new_one(package, name, permissions, mobileRxFg, mobileRxBg, wlanRxFg, wlanRxBg, num)

    def PUT(self, package, name='default', permissions='default', mobileRxFg='default', mobileRxBg='default', wlanRxFg='default', wlanRxBg='default', num='default'):
        app = self.apps.get_by_package(package)

        if name is not 'default':
            self.apps.applications.update({"_id": package}, {"$set": {'name': name}}, upsert=False)
        if permissions is not 'default':
            self.apps.applications.update({"_id": package}, {"$set": {'permissions': permissions}}, upsert=False)
        if mobileRxFg is not 'default':
            new = float(app['mobileRxFg']) + float(mobileRxFg)
            self.apps.applications.update({"_id": package}, {"$set": {'mobileRxFg': str(new)}}, upsert=False)
        if mobileRxBg is not 'default':
            new = float(app['mobileRxBg']) + float(mobileRxBg)
            self.apps.applications.update({"_id": package}, {"$set": {'mobileRxBg': str(new)}}, upsert=False)
        if wlanRxFg is not 'default':
            new = float(app['wlanRxFg']) + float(wlanRxFg)
            self.apps.applications.update({"_id": package}, {"$set": {'wlanRxFg': str(new)}}, upsert=False)
        if wlanRxBg is not 'default':
            new = float(app['wlanRxBg']) + float(wlanRxBg)
            self.apps.applications.update({"_id": package}, {"$set": {'wlanRxBg': str(new)}}, upsert=False)
        if num is not 'default':
            self.apps.applications.update({"_id": package}, {"$set": {'num': str(int(app['num']) + 1)}}, upsert=False)

    def DELETE(self, package):
        self.apps.remove_by_package(package)

if __name__ == '__main__':
    conf = {
        '/': {
             'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
             'tools.sessions.on': True,
             'tools.response_headers.on': True,
             'tools.response_headers.headers': [('Content-Type', 'text/plain')],
        }
    }
    cherrypy.quickstart(BatMonWebService(), '/', conf)
